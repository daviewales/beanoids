extends CollisionPolygon2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var screen_rect = get_viewport_rect()
	var top_left = screen_rect.position
	var bottom_right = screen_rect.end
	var top_right = Vector2(bottom_right.x, top_left.y)
	var bottom_left = Vector2(top_left.x, bottom_right.y)
	
	self.set_polygon(PoolVector2Array(
		[top_left, top_right, bottom_right, bottom_left]))
		



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
