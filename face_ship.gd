extends KinematicBody2D


export (int) var acceleration_factor = 5
export (int) var speed = 200
export (float) var rotation_speed = 5

var laser_scene = preload("res://Laser.tscn")
var bander_cheese_bullet_scene = preload("res://BanderCheeseBullet.tscn")
onready var game_node = get_parent()

var velocity = Vector2()
var rotation_dir = 0


func get_input():
	rotation_dir = 0
	if Input.is_action_pressed("right"):
		rotation_dir += 1
	if Input.is_action_pressed("left"):
		rotation_dir -= 1
	if Input.is_action_pressed("down"):
		velocity = velocity - acceleration_factor*Vector2(1, 0).rotated(rotation)
	if Input.is_action_pressed("up"):
		velocity = velocity + acceleration_factor*Vector2(1, 0).rotated(rotation)
	if Input.is_action_pressed("banderCheeseBullet"):
		var bander_cheese_bullet = bander_cheese_bullet_scene.instance()
		game_node.add_child(bander_cheese_bullet)
	if Input.is_action_just_pressed("laser"):
		var laser = laser_scene.instance()
		game_node.add_child(laser)


func _physics_process(delta):
	get_input()
	rotation += rotation_dir * rotation_speed * delta
	velocity = move_and_slide(velocity)

# Called when the node enters the scene tree for the first time.
func _ready():
	position = get_viewport_rect().size / 2
	# Start facing upwards
	#rotation = -PI/2


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


