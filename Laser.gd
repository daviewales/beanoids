extends RigidBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export (int) var laser_speed = 600


# Called when the node enters the scene tree for the first time.
func _ready():
	var face_ship = get_node("/root/Game/FaceShip")
	add_collision_exception_with(face_ship)
	self.rotation = face_ship.rotation
	self.position = face_ship.position + 55*Vector2(1,0).rotated(self.rotation)
	self.linear_velocity = laser_speed*Vector2(1,0).rotated(self.rotation)
	yield(get_tree().create_timer(0.1), "timeout")
	remove_collision_exception_with(face_ship)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	self.rotation = linear_velocity.angle()

func _on_VisibilityNotifier2D_viewport_exited(_viewport):
	queue_free()


func _on_Laser_Bolt_body_entered(body):
	print("laser:", body.name)
	if body.name.match("*Beanoid*"):
		queue_free()


func _on_Timer_timeout():
	queue_free()
