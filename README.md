# Beanoids

Just open `project.godot` in the [Godot](https://godotengine.org/) Game Engine!
If you add anything fun to the game, please send a merge request!
