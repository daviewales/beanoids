extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	position = get_viewport_rect().size / 2 # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	get_input()


func get_input():
	if visible:
		if (Input.is_action_just_pressed("ui_accept")
			|| Input.is_action_just_pressed("ui_cancel")):
			get_tree().change_scene("res://MainMenu.tscn")
