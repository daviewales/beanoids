extends Node

signal increment_score
signal destroy_face_ship

export (int) var MAX_BEANOIDS = 10

var beanoid_green_1 = preload("res://Beans/Pile of Beans (green 1).tscn")
var beanoid_navy_1 = preload("res://Beans/Pile of Beans (navy 1).tscn")
var beanoid_speckled_1 = preload("res://Beans/Pile of Beans (speckled 1).tscn")
var beanoid_white_1 = preload("res://Beans/Pile of Beans (white 1).tscn")
var beanoid_yellow_1 = preload("res://Beans/Pile of Beans (yellow 1).tscn")
var beanoid_yellow_2 = preload("res://Beans/Pile of Beans (yellow 2).tscn")

var beanoids = [
	beanoid_green_1,
	beanoid_navy_1,
	beanoid_speckled_1,
	beanoid_white_1,
	beanoid_yellow_1,
	beanoid_yellow_2,
	]
	

func add_random_beanoid():
	var beanoid_index = round(rand_range(0, len(beanoids) - 1))
	var beanoid = beanoids[beanoid_index]
	
	######################################################
	# Move the random position code out of the Beanoid into the Beanoid
	# Container (e.g. this script.)
	# Use the instructions from the 'Your first game tutorial' for spawning
	# mobs.
	######################################################
	$BeanoidPath/PathFollow2D.offset = randi()
	var beanoid_scene = beanoid.instance()
	add_child(beanoid_scene)
	beanoid_scene.connect("laser_beanoid_collision", self, "on_laser_beanoid_collision")
	beanoid_scene.connect("face_ship_beanoid_collision", self, "on_face_ship_beanoid_collision")
	# Set the beanoids's direction perpendicular to the path direction.
	var direction = $BeanoidPath/PathFollow2D.rotation + PI / 2
	# Set the beanoids's position to a random location.
	beanoid_scene.position = $BeanoidPath/PathFollow2D.position + 40*Vector2(1, 0).rotated(direction)
	# Add some randomness to the direction.
	direction += rand_range(-PI / 4, PI / 4)
	beanoid_scene.rotation = direction
	# Set the velocity (speed & direction).
	beanoid_scene.linear_velocity = Vector2(rand_range(20, 100), 0)
	beanoid_scene.linear_velocity = beanoid_scene.linear_velocity.rotated(direction)
	beanoid_scene.angular_velocity = rand_range(-3, 3)

func on_laser_beanoid_collision():
	emit_signal("increment_score")
	
func on_face_ship_beanoid_collision():
	emit_signal("destroy_face_ship")

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_Timer_timeout():
	if get_child_count() < MAX_BEANOIDS:
		add_random_beanoid()
