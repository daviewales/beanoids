extends RigidBody2D

signal face_ship_beanoid_collision
signal laser_beanoid_collision

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func set_random_position():
	var screen_rect = get_viewport_rect()
	var top_left = screen_rect.position
	var bottom_right = screen_rect.end
	position = Vector2(
		rand_range(top_left.x, bottom_right.x),
		rand_range(top_left.y, bottom_right.y))

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	set_random_position()
	linear_velocity = Vector2(rand_range(10, 50), rand_range(10, 50))


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Beanoid_body_entered(body):
	var collider = body.get_name()
	if not $CollisionPolygon2D.disabled && (collider == 'FaceShip'):
		emit_signal("face_ship_beanoid_collision")
	elif collider.match("*Laser Bolt*"):
		emit_signal("laser_beanoid_collision")
		hide()
		$CollisionPolygon2D.set_deferred("disabled", true)
		$AudioStreamPlayer2D.play()
		#visible = false
		yield($AudioStreamPlayer2D, "finished")
		queue_free()


