extends RigidBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


export (int) var bander_cheese_bullet_speed = 200


# Called when the node enters the scene tree for the first time.
func _ready():
	var face_ship = get_node("/root/Game/FaceShip")
	self.rotation = face_ship.rotation
	self.position = face_ship.position + 45*Vector2(1,0).rotated(self.rotation)
	self.linear_velocity = bander_cheese_bullet_speed*Vector2(1,0).rotated(self.rotation)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_VisibilityNotifier2D_viewport_exited(_viewport):
	queue_free()
